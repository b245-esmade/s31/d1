
// Node JS Introduction
	//used for building server-side applications
	// open-source, JS runtime environment for creating server-side applications
	// with node JS, JS now has access to the ff: System Resources such memory, file system, network

	// Benefits
	/*
		-Performance. Optimized for web applications
		-Familiarity. Same old JS
		-Access to Node Package Manager(NPM). World's largest registry of packages
	*/




// Client-server architecture
	// Clients
	// Internet
	// Server

	// Benefits
		/*
			-centralized data makes application more scalable and maintainable
			-multiple client apps may all use dynamically generated data
		*/



// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// HTTP is a protocol that allows the fetching of resources such as HTML documents

let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client
// The http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it

http.createServer(function (req, res) {

	//Use the writeHead() method to:
	//Set a status code for the response, a 200 means OK
	//Set the content-type of the response as a plain text message

	res.writeHead(200, {'Content-Type': 'text/plain'});
	// Send the response with text content 'Hello World'
	res.end("Hello, B245!");


// A port is a virtual point where network connections start and end.
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server

}).listen(4000);



//When server is running, console will print the message;

console.log('Server running at localhost:4000');





// Installing NPM and Nodemon
 	// Enter this command
		/*
		Windows
		npm install -g nodemon


		Linux
		sudo npm install nodemon -g

		*/


// How to call Nodemon Server (auto update/run/save)
	/*
		nodemon routes.js(name of the file)
	
	*/


//Error

Error: listen EADDRINUSE: address already in use :::4000
2 process are using 4000


// How to terminate already in use :::4000 error 
	//Enter this command npx kill-port [port-number]
	 npx kill-port 4000
