

const http = require('http');

// Creates a variable 'port' to store the port number
const port = 4000;

// Creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request, response) =>{

	// Accessing the "greeting" route returning a message of "Hello World"
	if (request.url =="/greeting"){
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end("Hello World");

	// Accessing the 'homepage' route returns a message of 'Welcome to our homepage'
	} else if (request.url =='/homepage'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to our homepage!');

	// All other routes will return a message of 'Page not available'
	}else {

		// Set a status code for the response - a 404 means "NOT FOUND"
		response.writeHead(404, {'Content-Type': "text/plain"})
		response.end('Page not available');
	}



})

// Uses the "server" and "port" variables created above.
server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);
