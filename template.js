const http = require('http');

const port = 3000;

const server = http.createServer((request, response) =>{
	if (request.url == '/login'){
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end("Welcome to the login page!");
	}else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not available');
	}
	
})

server.listen(port);
console.log(`Server now accessible at localhost: ${port}`);





//OR




let http = require("http");
http.createServer(function (req, res) {

res.writeHead(200, {'Content-Type': 'text/plain'});
res.end("Hello, B245!");


}).listen(4000);
console.log('Server running at localhost:4000');


// OR

const http = require('http');

	let port = 4000;

	http.createServer(function(req, res){
		if(req.url === "/" && req.method === "GET"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Welcome to the Booking System!");
				res.end();
			}
			else if(req.url === "/profile" && req.method === "GET"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Welcome to your profile!");
				res.end();
			}
			else {
				res.writeHead(404, {'Content-Type': 'text/plain'});
				res.write("Page Unavailable");
				res.end();
			}

	}).listen(port);

	console.log(`Server is running at port ${port}!`);
